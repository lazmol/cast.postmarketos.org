date: 2021-04-28
title: "#5 INTERVIEW: proycon (of Sxmo Fame)"
length: "28:54"
timestamps:
  - "00:00 Intro"
  - "00:25 Welcome"
  - "00:43 Alpineconf"
  - "01:57 Interview with proycon"
  - "27:27 Outro"
---

A different format for the episode this time, an interview with proycon, who is one
of the sxmo developers. Besides the interview there's an announcement of Alpineconf,
an online conference about Alpine Linux.

Send in your questions with #postmarketOSpodcast on Mastodon!

Featuring @craftyguy, @MartijnBraam, @PureTryOut, @dylanvanassche, @z3ntu, @proycon
(in order of appearance).

Referenced in this episode:

* [Alpineconf](https://gitlab.alpinelinux.org/alpine/alpineconf-cfp)
* sxmo links
    * [sxmo user documentation](https://git.sr.ht/~mil/sxmo-docs/tree/master/USERGUIDE.md)
    * [proycon's dotfiles repository](https://git.sr.ht/~proycon/dotfiles)
    * [how to contribute to sxmo](https://git.sr.ht/~mil/sxmo-docs/tree/master/CONTRIBUTING.md)
    * [example phone ring hook for sxmo](https://git.sr.ht/~proycon/dotfiles/tree/master/item/sxmo/hooks/ring)
    * [sxmo on archlinux arm by justinesmithies](https://github.com/justinesmithies/sxmo-alarm)
    * [sxmo demo video by Martijn](https://www.youtube.com/watch?v=z3dTrIa52O4)
    * sxmo can be found on <del>freenode</del> OFTC on #sxmo and #oftc_#sxmo:matrix.org for Matrix

Editing by: @MartijnBraam
