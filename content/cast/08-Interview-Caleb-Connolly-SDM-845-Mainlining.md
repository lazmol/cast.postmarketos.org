date: 2021-08-27
title: "#8 INTERVIEW: Caleb Connolly (of OnePlus 6 / SDM845 Mainlining Fame)"
length: "45:21"
timestamps:
  - "00:48 Who are you?"
  - "02:41 TTYescape"
  - "05:20 postmarketos-update-kernel"
  - "08:00 Custom Android bootloader"
  - "17:33 How did you get into this world?"
  - "27:28 WayDroid"
  - "32:00 What are you working on now?"
  - "37:00 Pre-built images"
  - "37:50 Conferences"
  - "39:10 Shout out"
  - "40:12 Studying, Linaro"
  - "42:21 Wrapping up"
---

Guess what, we did another interview episode - this time with Caleb! After
talking about various cool recovery systems that will make you feel like a true
hacker when used, we talk about how they got sucked into the free software
Linux smartphone scene. From how amazing it is to mainline your device and then
have the terminal appear on the screen for the first time, to how it's not
unlikely that their OnePlus 6 port becomes daily driver ready by the end of the
year.

Like always, write feedback with `#postmarketOSpodcast` to our
[Mastodon](https://fosstodon.org/@postmarketOS).

Featuring @craftyguy, @PureTryOut, @z3ntu, @MartijnBraam, @minlexx,
@ollieparanoid, @caleb (in order of apperance).

Some ways to reach Caleb:

* [Homepage](https://connolly.tech/)
* [Mastodon](https://fosstodon.org/@calebccff)
* [Patreon](https://www.patreon.com/calebccff) (for donations)
* [Linux OnePlus Chat](https://fosstodon.org/@calebccff/106830625502283374)

Referenced in this episode:

* TTYescape
    * [mastodon post with demo video](https://fosstodon.org/@calebccff/106523501346685288)
    * [wiki page](https://wiki.postmarketos.org/wiki/TTYescape)
    * [merge request](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2309)
    * try it out on pmOS edge: `apk add ttyescape`

* postmarketos-update-kernel:
    * support for A/B slots:
    	[!2214](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2214)
    * post-install triggers:
    	[!2215](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2215)
    * discussion to do this for all devices in community/main categories
      ([#1158](https://gitlab.com/postmarketOS/pmaports/-/issues/1158))
    * not mentioned in the episode, but related: making Android kernel update
      post processing distro-independent
      ([#1152](https://gitlab.com/postmarketOS/pmaports/-/issues/1152))

* Custom Android bootloader
    * [Caleb's blog post](https://connolly.tech/posts/2021_05_06-android-bootloaders/)
    * Mobile NixOS is able to
      [boot into various *generations*](https://github.com/NixOS/mobile-nixos/blob/master/doc/boot_process.adoc#booting-a-specific-generation)
      in their custom recovery mode

* How did you get into this world?
    * Inspired by
      [OnePlus 5](https://wiki.postmarketos.org/wiki/OnePlus_5_(oneplus-cheeseburger))
    * OnePlus 6 ports for
      [Sailfish OS](https://github.com/sailfish-oneplus6),
      [Ubuntu Touch](https://devices.ubuntu-touch.io/device/fajita/),
      [postmarketOS](https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada))
    * Mainlining guides/wiki pages:
      [SDM845 Mainlining](https://wiki.postmarketos.org/wiki/SDM845_Mainlining),
      [Mainlining Overview](https://wiki.postmarketos.org/wiki/Mainlining),
      [old guide](https://wiki.postmarketos.org/wiki/Mainlining_Guide)
    * [Phosh performance demo on OP6](https://fosstodon.org/@calebccff/106721344505860321)

* WayDroid
    * [Short demo](https://fosstodon.org/@calebccff/106636477530309968)
    * [Caleb playing crossy road with WayDroid](https://www.youtube.com/watch?v=I9qaD5YIPkc)
    * Let's package WayDroid for postmarketOS:
      [pma#1173](https://gitlab.com/postmarketOS/pmaports/-/issues/1173)
    * Shout out to WayDroid developers @erfanoabdi et al for making this
      possible, [upstream repositories](https://github.com/waydroid/)

* What are you working on now / how can people help out?
    * [Mastodon Thread](https://fosstodon.org/@calebccff/106722370514563683)
      with cool starter programming projects for people owning a OnePlus
      5/6/(7?), including the dash daemon mentioned in the episode.

* [Mobian announcing official support for the OnePlus 6/6T/Pocophone F1](https://blog.mobian-project.org/posts/2021/05/17/update-2021-05-17/)

Editing by: @MartijnBraam
