date: 2021-03-29
title: "#4 EG25-G, v21.03, Why Alpine, mobile-config-firefox"
length: "32:31"
timestamps:
  - "00:00 Intro"
  - "00:28 Welcome"
  - "00:50 Dylan joins the postmarketOS team"
  - "01:47 EG25-G modem improvements"
  - "04:16 postmarketOS v21.03 Release"
  - "08:10 Mainline for MSM8974 devices"
  - "09:27 Feedbackd"
  - "10:31 Wiki: applications by category"
  - "12:00 install-makedepends"
  - "14:10 Martijn's kitchen counter"
  - "18:11 N900 PowerVR SGX Acceleration"
  - "20:41 What is so great about Alpine Linux?"
  - "24:13 mobile-config-firefox"
  - "30:16 Outro"
---

Dylan joins the team! And besides the usual bunch of news, we learn how
Martijn's kitchen counter is involved with the upcoming homepage redesign. The
questions section covers what's so great about Alpine, and what
mobile-config-firefox is all about.

Send in your questions with #postmarketOSpodcast on Mastodon!

Featuring @craftyguy, @MartijnBraam, @PureTryOut, @z3ntu, @ollieparanoid,
@dylanvanassche (in order of appearance).

Referenced in this episode:

* [Dylan Van Assche regarding Sailfish: End of an era](https://dylanvanassche.be/blog/2020/end-of-an-era/)
* [pine64-pinephone: eg25-g modem improvements (now merged!)](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/1931)
* v21.03 Release
    * [Rename release channels to the branch names, except for edge](https://gitlab.com/postmarketOS/pmbootstrap/-/issues/2015)
    * Release will be out on 31st of March, check
      [postmarketos.org](https://postmarketos.org) then for more
    information :)
* [main/linux-postmarketos-qcom-msm8974: upgrade to 5.11.10](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2015)
* [feedbackd-device-themes](https://source.puri.sm/Librem5/feedbackd-device-themes)
* [Wiki: Applications by category](https://wiki.postmarketos.org/wiki/Applications_by_category)
    * Contributed by: @alpabrz, @Newbyte, @Railroadmanualjimmy, @ollieparanoid
    * [related discussion](https://gitlab.com/postmarketOS/wiki/-/issues/59)
* [install-makedepends](https://gitlab.com/postmarketOS/install-makedepends)
* [PowerVR SGX Acceleration](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/1868)
* Why Alpine
    * [Alpine Releases](https://alpinelinux.org/releases/) and their support timeframes
    * [Building a distro with musl libc: Why and how Alpine Linux did it](https://archive.fosdem.org/2017/schedule/event/building_a_distro_with_musl_libc/) (Video, FOSDEM 2017)
    * Alpine's package manager doing well in [Linux package managers are slow](https://michael.stapelberg.ch/posts/2019-08-17-linux-package-managers-are-slow/) (2019)
        * Allan McRae from Arch Linux pointed out that the benchmarks are not
	  accurate though (didn't know that at time of recording), hopefully
	  the author does them again at some point (looks like it form the
	  notes on top of the blog post).
	* From experience we know that Alpine's package manager is one of the
	  fastest out there.
        * For people curious about optimizing package manager speed, check out
	  [distri](https://distr1.org/)
* [mobile-config-firefox](https://gitlab.com/postmarketOS/mobile-config-firefox/)
    * Using [Firefox Remote Debugger](https://gitlab.com/postmarketOS/mobile-config-firefox/#contributing-changes-to-userchrome) with mobile Linux distros, to edit Firefox UI CSS with the Element
      Inspector etc.
    * [Upstream discussion](https://bugzilla.mozilla.org/show_bug.cgi?id=1579348)

Editing by: @craftyguy
